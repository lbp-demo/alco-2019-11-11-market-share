#---
# Load
# BSP Deposit Data for Philippine Banking Industry
# ---

source("scripts/create-libs.R")
source("scripts/create-paths.R")

bsp.filename <- "2.2 consistent-cols.xls"
bsp.filepath <- file.path(data.repo, "BSP", bsp.filename)
bsp.raw <- read_xls(bsp.filepath)

# View bsp.raw
View(bsp.raw)

# Inspect bsp.raw metadata
str(bsp.raw)

#Read the data
bsp.raw <- read_xls(bsp.filepath, 
                    sheet = "Regional & Provincial",
                    range = "C47:J543",
                    col_names = c("Region", "Province", "Bank_Type", "Total", 
                                  "Demand", "Savings", "Time", "Others")
)

# View bsp.raw
View(bsp.raw)

#Fill NA Entries in Region column
marketshare.temp00 <- bsp.raw %>% 
  fill(Region, .direction = "down")

# View marketshare.temp00
View(marketshare.temp00)

#Fill NA Entries in Province column, Grouped by Region
marketshare.temp01 <- marketshare.temp00 %>% 
  group_by(Region) %>%  fill(Province, .direction = "down") %>% ungroup()

#Remove NA in Province column and Remove Total Column
marketshare.temp02 <- marketshare.temp01 %>% 
  filter(!is.na(Province)) %>% select(-"Total")

#Remove Bank Types except with NA values in the Bank Type Column
marketshare.temp03 <- marketshare.temp02 %>% 
  filter(is.na(Bank_Type))


#Gather Deposit type Columns into a single column
marketshare.temp04 <- marketshare.temp03 %>% 
  gather("Deposit_Type", "Amount","Demand", "Savings", "Time", "Others")


# Remove NA in Amount and remove Bank Type Column
marketshare.temp05 <-marketshare.temp04 %>% 
  filter(!is.na(Amount))  %>% select(-"Bank_Type") 

#Change data type of AMOUNT to Numeric
marketshare.temp06 <- marketshare.temp05 %>% 
  mutate(Amount = as.numeric(`Amount`))


marketshare.df <- marketshare.temp06


#Checking total values
marketshare.df %>% 
  group_by (Region) %>% 
  summarise(
    Total = sum(Amount)
  )


#Remove bsp.raw and marketshare.temps
rm(bsp.raw)
rm(marketshare.temp00)
rm(marketshare.temp01)
rm(marketshare.temp02)
rm(marketshare.temp03)
rm(marketshare.temp04)
rm(marketshare.temp05)
rm(marketshare.temp06)


#Output RDS File

marketshare.outfile <- "marketshare.rds"
marketshare.outpath <- file.path(data.cache, marketshare.outfile)
saveRDS(marketshare.df, marketshare.outpath)

